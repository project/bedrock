<?php

/**
 * @file
 * Template for Bedrock Limestone.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   -- Content Header ['content_header']
 *   -- Content Main ['content_main']
 *   -- Column Left ['column_left']
 *   -- Column Right ['column_right']
 */
?>

<div class="panel-display limestone clearfix <?php if (!empty($class)): print $class; endif; ?>" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>

  <div class="limestone-content-container">

    <?php if ($content['content_header']): ?>
      <div class="limestone-content-header limestone-content clearfix panel-panel">
          <?php print $content['content_header']; ?>
      </div><!-- /.limestone-content-header -->
    <?php endif; ?>

    <?php if ($content['content_main']): ?>
      <div class="limestone-content-main limestone-content clearfix panel-panel">
          <?php print $content['content_main']; ?>
      </div><!-- /.limestone-content-main -->
    <?php endif; ?>

    <div class="limestone-column-container clearfix">

      <div class="limestone-column-left limestone-column panel-panel">
        <div class="limestone-column-left-inner limestone-column-inner panel-panel-inner">
          <?php print $content['column_left']; ?>
        </div><!-- /.limestone-column-left-inner -->
      </div><!-- /.limestone-column-left -->

      <div class="limestone-column-right limestone-column panel-panel">
        <div class="limestone-column-right-inner limestone-column-inner panel-panel-inner">
          <?php print $content['column_right']; ?>
        </div><!-- /.limestone-column-right-inner -->
      </div><!-- /.limestone-column-right -->

    </div><!-- /.limestone-column-container -->

  </div><!-- /.limestone-content-container -->

</div><!-- /.limestone -->
