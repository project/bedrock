<?php

/**
 * @file
 * Bedrock Limestone panels layout plugin.
 */

// Plugin definition
$plugin = array(
  'title' => t('Limestone'),
  'icon' => 'limestone.png',
  'category' => t('Bedrock'),
  'theme' => 'limestone',
  'admin css' => 'admin.limestone.css',
  'regions' => array(
    'content_header' => t('Content Header'),
    'content_main' => t('Content Main'),
    'column_left' => t('Column Left'),
    'column_right' => t('Column Right'),
  ),
);
