<?php

/**
 * @file
 * Bedrock Comendite panels layout plugin.
 */

// Plugin definition
$plugin = array(
  'title' => t('Riebeckite'),
  'icon' => 'riebeckite.png',
  'category' => t('Bedrock'),
  'theme' => 'riebeckite',
  'admin css' => 'admin.riebeckite.css',
  'regions' => array(
    'content_header' => t('Content Header'),
    'content_main_column' => t('Content Main Column'),
    'content_secondary_column' => t('Content Secondary Column'),
  ),
);
