<?php

/**
 * @file
 * Template for Bedrock Riebeckite.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   -- Content Header ['content_header']
 *   -- Content Main Column ['content_main_column']
 *   -- Content Secondary Column ['content_secondary_column']
 */
?>

<div class="panel-display riebeckite clearfix <?php if (!empty($class)): print $class; endif; ?>" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>

  <section class="riebeckite-content-container">

    <?php if ($content['content_header']): ?>
      <div class="riebeckite-content-header clearfix panel-panel">
          <?php print $content['content_header']; ?>
      </div><!-- /.riebeckite-content-header -->
    <?php endif; ?>

    <div class="riebeckite-content-column-container clearfix <?php if ($content['content_secondary_column']): print 'with-secondary-column'; endif; ?>">

      <?php if ($content['content_secondary_column']): ?>
        <aside class="riebeckite-content-secondary-column riebeckite-column panel-panel">
          <div class="riebeckite-content-secondary-column-inner riebeckite-column-inner panel-panel-inner">
            <?php print $content['content_secondary_column']; ?>
          </div><!-- /.riebeckite-content-secondary-column-inner -->
        </aside><!-- /.riebeckite-content-secondary-column -->
      <?php endif; ?>

      <article class="riebeckite-content-main-column riebeckite-column panel-panel">
        <div class="riebeckite-content-main-column-inner riebeckite-column-inner panel-panel-inner">
          <?php print $content['content_main_column']; ?>
        </div><!-- /.riebeckite-content-main-column-inner -->
      </article><!-- /.riebeckite-content-main-column -->

    </div><!-- /.riebeckite-content-column-container -->

  </section><!-- /.riebeckite-content-container -->

</div><!-- /.riebeckite -->
