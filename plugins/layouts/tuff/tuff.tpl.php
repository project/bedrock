<?php

/**
 * @file
 * Template for Bedrock Tuff.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   -- Sidebar ['sidebar']
 *   -- Content Main ['content_main']
 */
?>

<div class="panel-display tuff clearfix <?php if (!empty($class)): print $class; endif; ?>" <?php if (!empty($css_id)): print "id=\"$css_id\""; endif; ?>>

  <section class="tuff-content-main panel-panel">
    <div class="tuff-content-main-inner panel-panel-inner">
      <?php print $content['content_main']; ?>
    </div><!-- /.tuff-content-main-inner -->
  </section><!-- /.tuff-content-main -->

  <section class="tuff-sidebar panel-panel">
    <div class="tuff-sidebar-inner panel-panel-inner">
      <?php print $content['sidebar']; ?>
    </div><!-- /.tuff-sidebar-inner -->
  </section><!-- /.tuff-sidebar -->

</div><!-- /.tuff -->
