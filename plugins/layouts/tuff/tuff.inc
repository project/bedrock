<?php

/**
 * @file
 * Bedrock Tuff panels layout plugin.
 */

// Plugin definition
$plugin = array(
  'title' => t('Tuff'),
  'icon' => 'tuff.png',
  'category' => t('Bedrock'),
  'theme' => 'tuff',
  'admin css' => 'admin.tuff.css',
  'regions' => array(
    'sidebar' => t('Sidebar'),
    'content_main' => t('Content Main'),
  ),
);
