<?php
/**
 * @file
 * Theme functions for the Views hover display.
 */

/**
 * Prepare variables for hover template.
 */
function template_preprocess_bedrock_views_view_fields(&$vars) {
  // Use parent preprocessor.
  template_preprocess_views_view_fields($vars);

  // Setup the 3 groups of fields.
  $vars['exposed_fields'] = array();
  $vars['trigger_field'] = array();
  $vars['dropdown_fields'] = array();
  foreach ($vars['fields'] as $key => $field) {
    if ($key == $vars['options']['dropdown_config']['trigger_element']) {
      // This field should be inside the trigger element.
      $vars['trigger_field'][$key] = $field;
    }
    elseif (isset($vars['options']['dropdown_config']['in_dropdown'][$key]) && $vars['options']['dropdown_config']['in_dropdown'][$key]) {
      // These fields are in the dropdown.
      $vars['dropdown_fields'][$key] = $field;
    }
    else {
      // These fields are exposed normally.
      $vars['exposed_fields'][$key] = $field;
    }

    unset($vars['fields'][$key]);
  }

  // Need a unique ID.
  static $id = 1;
  $vars['dropdown_id'] = 'bedrock-dropdown-' . $id;
  $id++;

  $vars['data_options'] = !empty($vars['options']['dropdown_config']['hover']) ? 'is_hover:true' : FALSE;
}
