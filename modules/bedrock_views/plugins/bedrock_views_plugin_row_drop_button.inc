<?php
/**
 * @file
 * Contains the Views dropdown button row plugin.
 */

/**
 * The dropdown button row plugin.
 */
class bedrock_views_plugin_row_drop_button extends views_plugin_row_fields {

  /**
   * {@inheritdoc}
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['dropdown_config'] = array(
      'trigger_element' => array('default' => array()),
      'in_dropdown' => NULL,
      'hover' => array('default' => FALSE),
    );
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $fields = $this->display->handler->get_field_labels();

    $form['dropdown_config'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dropdown Button Configuration'),
      '#description' => t('Choose one field to be inside the trigger element, and the fields that should appear in the dropdown. Any field not selected as trigger or in dropdown will appear in the row normally.'),
      '#collapible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['dropdown_config']['trigger_element'] = array(
      '#type' => 'radios',
      '#options' => $fields,
      '#title' => t('Trigger Element'),
      '#default_value' => $this->options['dropdown_config']['trigger_element'],
      '#description' => t('Select one field to be inside the trigger element. This field should must not be linked in the views field configuration or the dropdown will not work.'),
    );
    $form['dropdown_config']['in_dropdown'] = array(
      '#type' => 'checkboxes',
      '#options' => $fields,
      '#title' => t('In dropdown'),
      '#default_value' => $this->options['dropdown_config']['in_dropdown'],
      '#description' => t('Select fields that should appear in the dropdown. If the trigger element is selected it will have no effect, it will only appear in the trigger, not the dropdown. '),
    );
    $form['dropdown_config']['hover'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display on hover?'),
      '#default_value' => $this->options['dropdown_config']['hover'],
      '#description' => t('If selected, contents in the dropdown will appear on hover. Otherwise, on click.'),
    );
  }

  /**
   * {@inheritdoc}
   */
  function options_validate(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    // We cannot allow *all* fields to be in the dropdown.
    $in_dropdown = array();
    foreach ($form_state['values']['row_options']['dropdown_config']['in_dropdown'] as $key => $enabled) {
      if ($enabled) {
        $in_dropdown[$key] = $enabled;
      }
    }
    if (count($in_dropdown) && (count($in_dropdown) == count($form['dropdown_config']['in_dropdown']['#options']))) {
      form_set_error('dropdown_config][in_dropdown', t('At least one field must *not* be in the dropdown.'));
    }

    // Ensure a trigger element was selected.
    if (is_null($form_state['values']['row_options']['dropdown_config']['trigger_element'])) {
      form_set_error('dropdown_config][trigger_element', t('Choose a field to appear as the trigger element.'));
    }
  }

}
