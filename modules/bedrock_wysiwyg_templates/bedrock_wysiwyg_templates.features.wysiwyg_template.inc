<?php
/**
 * @file
 * bedrock_wysiwyg_templates.features.wysiwyg_template.inc
 */

/**
 * Implements hook_wysiwyg_template_default_templates().
 */
function bedrock_wysiwyg_templates_wysiwyg_template_default_templates() {
  $templates = array();
  $templates['foudation_tabs'] = array(
    'title' => 'Foundation Tabs',
    'description' => '',
    'weight' => 0,
    'fid' => 0,
    'body' => '<dl class="tabs" data-tab=""><dd class="tab-title active"><a href="#panel2-1">Tab 1</a></dd><dd class="tab-title"><a href="#panel2-2">Tab 2</a></dd><dd class="tab-title"><a href="#panel2-3">Tab 3</a></dd><dd class="tab-title"><a href="#panel2-4">Tab 4</a></dd></dl><div class="tabs-content"><div class="content active" id="panel2-1"><p>First panel content goes here...</p></div><div class="content" id="panel2-2"><p>Second panel content goes here...</p></div><div class="content" id="panel2-3"><p>Third panel content goes here...</p></div><div class="content" id="panel2-4"><p>Fourth panel content goes here...</p></div></div>',
    'format' => 'full_html',
    'name' => 'foudation_tabs',
    'content_types' => array(),
  );
  $templates['foundation_accordion'] = array(
    'title' => 'Foundation Accordion',
    'description' => '',
    'weight' => 0,
    'fid' => 0,
    'body' => '<dl class="accordion" data-accordion=""><dd class="accordion-navigation"><a href="#panel1">Accordion 1</a><div class="content active" id="panel1">Panel 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div></dd><dd class="accordion-navigation"><a href="#panel2">Accordion 2</a><div class="content" id="panel2">Panel 2. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div></dd><dd class="accordion-navigation"><a href="#panel3">Accordion 3</a><div class="content" id="panel3">Panel 3. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div></dd></dl>',
    'format' => 'full_html',
    'name' => 'foundation_accordion',
    'content_types' => array(),
  );
  $templates['foundation_orbit_slider'] = array(
    'title' => 'Foundation Orbit Slider',
    'description' => '',
    'weight' => 0,
    'fid' => 0,
    'body' => '<ul class="example-orbit" data-orbit=""><li class="active"><img alt="slide 1" src="http://placehold.it/1200x350"><div class="orbit-caption">Caption One.</div></li><li><img alt="slide 2" src="http://placehold.it/1200x350"><div class="orbit-caption">Caption Two.</div></li><li><img alt="slide 3" src="http://placehold.it/1200x350"><div class="orbit-caption">Caption Three.</div></li></ul>',
    'format' => 'full_html',
    'name' => 'foundation_orbit_slider',
    'content_types' => array(),
  );
  return $templates;
}
